﻿using UnityEngine;
using System.Collections;

public class AbilityClass{

    string name;
    string type;
    ParticleSystem particle;
    int aoe_radius;

    int value;
    int cost;

    //constructor single target abilities
    public AbilityClass(string name, string type, int cost, int value, ParticleSystem particle)
    {
        this.name = name;
        this.type = type;
        this.value = value;
        this.particle = particle;
        this.cost = cost;
    }

    //constructor for area effect abilities
    public AbilityClass(string name, string type, int cost, int value, int aoe_radius, ParticleSystem particle)
    {
        this.name = name;
        this.type = type;
        this.value = value;
        this.particle = particle;
        this.cost = cost/aoe_radius;
        this.aoe_radius = aoe_radius;
    }

    public string GetName { get { return name; } }
    public string GetType { get { return type; } }
    public int GetValue { get { return value; } }
    public int GetCost { get { return cost; } }
    public int GetRadius { get { return aoe_radius; } }

    public ParticleSystem GetAbilityParticle() 
    {
        return particle;
    }
}
