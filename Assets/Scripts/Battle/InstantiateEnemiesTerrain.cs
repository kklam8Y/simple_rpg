﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class InstantiateEnemiesTerrain : MonoBehaviour {

    //enemy prefabs
    public GameObject 
        skeleton, 
        hell_feind;

    //terrain prefabs
    public TerrainData 
        outdoor, 
        cave;

    //current location in the world
    string location;

    public Material SkyboxCave;

    //the fairy particle effect that illuminates you way in the dark cave.
    public Transform ParticleTrail;

    //light sources
    public Light light;
    public Light lightDirect;

    public void Initiate()
    {
        int i;
        int n = Random.Range(1, 4); // number of enemies to create

        float spacing = 0;

        if (n == 2)
            spacing = -1.0f;
        else if (n == 3)
            spacing = -2.0f;
        else if (n == 4)
            spacing = -3.0f;

        //Debug.Log(n);

        GetComponent<BattleManager>().enemies = new GameObject[n];

        location = PlayerPrefs.GetString("location");

        Debug.Log(location);

        for (i = 0; i < n; i++)
        {
            int ran = Random.Range(1, 4);
           // Debug.Log(ran);

            if (ran <= 1)
                GetComponent<BattleManager>().enemies[i] = Instantiate(skeleton, new Vector3(spacing, 0, 3), Quaternion.Euler(0, 180, 0)) as GameObject;
            else  if (ran > 1)
                GetComponent<BattleManager>().enemies[i] = Instantiate(hell_feind, new Vector3(spacing, 0, 3), Quaternion.Euler(0, 0, 0)) as GameObject;
            else
               GetComponent<BattleManager>().enemies[i] = null;

            spacing += 2.5f;
        }


        //load the approapriate terrain based on location in ht gameworld
        GameObject map = null;
        GameObject spec= null;

        if (location == "town 1")
        {
            map = Terrain.CreateTerrainGameObject(outdoor);
            //lightDirect.transform.Rotate(new Vector3(90, 0, 0));
           // RenderSettings.ambientLight = new Color(f, 1f, 1f);
        }
        else if (location == "cave 1")
        {
            map = Terrain.CreateTerrainGameObject(cave);
            lightDirect.enabled = false;
            spec = Instantiate(ParticleTrail, new Vector3(3.09f, 2.0f, -3.35f), Quaternion.identity) as GameObject;
            RenderSettings.skybox = SkyboxCave;
            RenderSettings.ambientLight = new Color(0.0f, 0.0f, 0.0f);
            light.range = 16.08478f;
        }

        Vector3 TS = map.GetComponent<Terrain>().terrainData.size;
        map.transform.position = new Vector3(-TS.x / 2, 0, -TS.z / 2);
         
    }
}
