﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CharacterClass
{
    public CharacterClass Target;
    public float damage_meter ;

    protected string name;
    protected int level;

    protected int experience;

    protected string id;

    //randed 1 or melee 0
    protected int ranged;

    //resources.
    protected float health;
    protected float mana;

    protected float max_health;
    protected float max_mana;

    //bool to check if char is alive.
    private bool dead;

    //stats primary/core
    protected float
        strength,
        agility,
        intellect,
        spirit,
        vitality;

    //stats secontary/offensive
    protected float
        attack, //damage from basic attack.
        m_attack, //damage from magic ability.
        speed; //affects order on turn execution.

    //stats secondary/defensive
    protected float
        p_resist, //damage mitigation from basica attacks.
        m_resist; //damage mitigation from abilities.
    protected float evasion, critical; //chance of not being hit % and chance of dealing 2x damage from attack %

    //choice to be executed at the end of the turn 
    public string choice;

    //names of animations for each character
    protected string[] animationsNames;

    public AbilityClass[] abilities;

    public BattleManager bm;

    protected List<AbilityClass> conditions = new List<AbilityClass>();

    //default constructor
    public CharacterClass()
    {
        abilities = new AbilityClass[0];
    }

    //condtructor used for enemies with logic
    public CharacterClass(BattleManager bm)
    {
        this.bm = bm;
    }

    public CharacterClass(string name, int level, float strength, float agility, float intellect, float spirit, float vitality, bool dead)
    {
        this.name = name;
        this.level = level;

        this.strength = strength;
        this.agility = agility;
        this.intellect = intellect;
        this.spirit = spirit;
        this.vitality = vitality;

        //initiate recourse statistics based on core statistics.
        health = (vitality * 10);
        mana = (spirit * 10);

        //secondary statistics can be further modified by items and/or buffs/debuffs.

        //initiate offensive statistics based on core statistics.
        attack = strength;
        m_attack = intellect;
        speed = agility;

        //initiate defensive statistics based on core statistics.
        p_resist = strength;
        m_resist = intellect;
        evasion = agility/100f;

        this.dead = dead;
    }

    public void ShowInfo() //char information
    {
        Debug.Log("/////////////////////////");
        Debug.Log("name: " + name);
        Debug.Log("level: " + level);
        Debug.Log("health: " + health);
        Debug.Log("mana: " + mana);
        Debug.Log("attack: " + attack);
        Debug.Log("magic attack " + m_attack);
        Debug.Log("physical resist: " + p_resist);
        Debug.Log("magic resist: " + m_resist);
        Debug.Log("evasion: " + evasion.ToString("F3"));
        Debug.Log("speed: " + speed);
    }

    public bool DoDamagePhysical(CharacterClass target) 
    {
        bool success;

        float damage = attack - target.p_resist;

        damage = Mathf.RoundToInt(damage);

		if (damage < 0)
			damage = 0;

        if (Random.Range(0.00f, 99.99f) > target.evasion)
        {
            if (Random.Range(0.0f, 99.99f) < critical)
            {

                damage *= 2;
                target.Health -= damage;

                if (target.health < 0)
                    target.health = 0;

                Debug.Log(name + " criticaly strikes " + target.name + " for " + damage);
                Debug.Log(target.name + " now has " + target.Health + "hp.");
            }
            else
            {
                target.Health -= damage;

                if (target.health < 0)
                    target.health = 0;

                Debug.Log(name + " attacks " + target.name + " for " + damage);
                Debug.Log(target.name + " now has " + target.Health + "hp.");
            }

            success = true;
        }

        else
        {        
            Debug.Log(name + " has missed attack on " + target.name);
            success = false;
        }

        target.damage_meter = damage;
        return success;
    }

    public bool DoDamageMagical(CharacterClass target, AbilityClass ability)
    {
        bool success;

        float damage = 0,
               heal = 0;

        if (mana >= ability.GetCost)
        {
            if (ability.GetType == "damage")
            {
                damage = (ability.GetValue + m_attack) - target.m_resist;
                if (damage > 0)
                    target.Health -= Mathf.RoundToInt(damage);
                else
                    target.Health -= 0;

            }
            else if (ability.GetType == "heal")
            {
                heal = ability.GetValue + m_attack;
                target.health += Mathf.RoundToInt(heal);
                if (target.health > target.max_health)
                    target.health = target.max_health;
            }
            else if (ability.GetType == "revival")
            {
                if (target.dead)
                    target.health = target.max_health;
            }

            mana -= ability.GetCost;

            if (target.health < 0)
                target.health = 0;

            Debug.Log(name + " casts " + ability.GetName + " on " + target.name);
            Debug.Log(target.name + " now has " + target.Health + "hp.");

            success = true;
        }
        else
        {
            Debug.Log(name + " casts " + ability.GetName + " on " + target.name);
            Debug.Log("not enough mana!");

            success = false;
        }

        return success;
    }

    //getters/setters

    //identity
    public string Name { get { return name; } set { name = value; } }
    public int Level { get { return level; } set { level = value; } }
    public string ID { get { return id; } }
    public int Experience { get { return experience; } set { experience = value; } }

    //resources
    public float Health { get { return health; } set { health = value; } }
    public float Mana { get { return mana; } set { mana = value; } }

    public float MaxHealth { get { return max_health; } set { max_health = value; } }
    public float MaxMana { get { return max_mana; } set { max_mana = value; } }

    //offensive
    public float Attack { get { return attack; } set { attack = value; } }
    public float M_Attack { get { return m_attack; } set { m_attack = value; } }
    public float P_Resist { get { return p_resist; } set { p_resist = value; } }
    public float M_Resist { get { return m_resist; } set { m_resist = value; } }

    //defensive
    public float Evasion { get{ return evasion;} set{evasion = value ;}}
    public bool isDead { get { return dead; } set { dead = value; } }

    public float Speed { get { return speed; } set { speed = value; } }


    //retrieve the animation stored in the animation string array
    public string GetAnimation(int element)
    {
        return animationsNames[element];
    }

    //get range
    public int GetRange()
    {
        return ranged;
    }

    public int GetAbilitiesLength()
    {
        return abilities.Length - 1;
    }

    public AbilityClass GetAbility(int ability_num)
    {
        return abilities[ability_num];
    }

    public string GetAbilityDetails(int ability_num)
    {
        string output = null;

        string ability_type = GetAbility(ability_num).GetType;

        if (ability_type == "damage")
            output += "<color=purple>" + abilities[ability_num].GetName + "</color>";
        else if(ability_type == "heal")   
            output += "<color=lime>" + abilities[ability_num].GetName + "</color>";
        else if(ability_type == "revival")
            output += "<color=orange>" + abilities[ability_num].GetName + "</color>";

        //include magic attack variable which is responsible for the abilities damage scaling
        int actual_value = abilities[ability_num].GetValue + (int)m_attack;

        return "Name: " + output + "\n" + "Value: " + "<color=silver>" + actual_value + "</color>" + 
           "\n" +"Cost: " + "<color=cyan> " + abilities[ability_num].GetCost + "</color>";
    }

    public virtual CharacterClass GetLogic(int current)
    {
        return Target;
    }

    public virtual void StatisticPerLevelGrowth() 
    {
        max_health = health = vitality * 10;
        max_mana = mana = spirit * 10;

        speed = agility;
        evasion = Mathf.Sqrt(agility);
        critical = Mathf.Sqrt(agility) * 2;
        //Debug.Log(critical);
    }

    public float ExperienceRequiredForLevelUp()
    {
        return Mathf.Pow((float)level * (float)level, 2);
    }

    public virtual int ExperienceDrop()
    {
        return 0;
    }

    public virtual int MoneyDrop()
    {
        return 0;
    }

}
