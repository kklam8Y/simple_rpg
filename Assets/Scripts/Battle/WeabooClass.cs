﻿using UnityEngine;
using System.Collections;

public class WeabooClass : CharacterClass {

    public WeabooClass(ParticleSystem[] ps)
    {
        level = PlayerPrefs.GetInt("taichi_lvl");
        experience = PlayerPrefs.GetInt("taichi_exp");

        name = "Tight Chi";
        id = "plr_tai";

        StatisticPerLevelGrowth();

        health = PlayerPrefs.GetFloat("taichi_hp");

        animationsNames = new string[6];

        animationsNames[0] = "idle_00";
        animationsNames[1] = "walk_00";
        animationsNames[2] = "punch_20";
        animationsNames[3] = "damage_23";
        animationsNames[4] = "special_20";
        animationsNames[5] = "liedown_06";


        abilities = new AbilityClass[ps.Length];

        abilities[0] = new AbilityClass("Rebirth", "revival", 50, 0, ps[0]);
    }

    public override void StatisticPerLevelGrowth()
    {
        strength = 5 + level * 3;
        agility = 5+ level * 3;
        intellect = 4 + level * 3;
        spirit = 6 +  level * 3;
        vitality = 4 + level * 4;

        attack = strength/2 + agility/2 + intellect/2;
        m_attack = intellect/2 + spirit;

        p_resist = strength / 2 + agility / 4;
        m_resist = intellect/2 + spirit;

        base.StatisticPerLevelGrowth();
    }
}
