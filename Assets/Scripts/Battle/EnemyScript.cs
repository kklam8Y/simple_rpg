﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Utility;
using UnityEngine.UI;

public class EnemyScript: MonoBehaviour{

    CharacterClass enemy, t;
    GameObject target;

    BattleManager bm;

    public ParticleSystem[] particles;
    ParticleSystem particle_current;

    public Vector3 origPos;
    Quaternion origRot;

    private int ability_num;
    public bool inTransit;
    public string targetType;

    public Canvas damage_meter_UI;
    public Canvas damage_meter_UI_current;

	// Use this for initialization
	void Start () 
    {
        origPos = transform.position;
        origRot = transform.rotation;

        bm = GameObject.FindGameObjectWithTag("BattleManager").GetComponent<BattleManager>();
    
        if (name == "skeleton(Clone)")
        {
            CreateEnemySkeletonWarrior();
        }
        if (name == "hellfiend(Clone)")
        {
            CreateEnemyHellFiend();
        }

        //enemy.GetBattleManager(bm);

        //enemy.ShowInfo();
	}
	
	// Update is called once per frame
	void Update () 
    {
        UpdatePlayerDeadStatus();
	}

    public void ExecuteChoice()
    {

        if (target != null)
        {
            CheckTarget();
            
           // damage_meter_UI.GetComponentInChildren<Text>().CrossFadeAlpha(-0.1f, 2, true);
        }
      

        if (enemy.choice == "attack")
        {
            inTransit = true;
            PerformAttack();
        }
        else if (enemy.choice == "defend")
        {
            inTransit = true;
            PerformDefend();
  
        }
        else if (enemy.choice == "ability")
        {
            inTransit = true;
            PerformAbility();
        }
        else if(enemy.choice == "none")
        {
            inTransit = false;              
        }
    }

    public CharacterClass getCharacter() { return enemy; }
    public GameObject Target { get { return target; } set { target = value; } }
    public int AbilityNumber { set { ability_num = value; } }

    void CreateEnemySkeletonWarrior()
    {
        enemy = new SkeletonClass(bm, particles, 2);
    }

    void CreateEnemyHellFiend()
    {
        enemy = new HellFiendClass(bm);
    }

    void CheckTarget()
    {
        if (targetType == "player")
            t = target.GetComponent<PlayerScript>().getCharacter();
        if (targetType == "enemy")
            t = target.GetComponent<EnemyScript>().getCharacter();
        if (t.isDead && enemy.choice == "attack")
        {
            enemy.choice = "none";
            Debug.Log("target is dead");
        }
    }

    void PerformAttack()
    {
        if (Vector3.Distance(transform.position, target.transform.position) > 1.0)
        {
            Quaternion rotation = transform.rotation;

            Vector3 reletivePos = target.transform.position - transform.position;

            if (name == "hellfiend(Clone)")
            {
                rotation = Quaternion.LookRotation(-reletivePos);
            }
            else
                rotation = Quaternion.LookRotation(reletivePos);

            Quaternion current = transform.localRotation;

            transform.localRotation = Quaternion.Slerp(current, rotation, 1 * Time.deltaTime);
            transform.position = Vector3.Lerp(transform.position, target.transform.position, Time.deltaTime);

            GetComponent<Animation>().Play(enemy.GetAnimation(1));
        }
        else if (Vector3.Distance(transform.position, target.transform.position) < 1.0)
        {
            GetComponent<Animation>().Play(enemy.GetAnimation(2));
        }
    }

    void PerformDefend()
    {
        GetComponent<Animation>().Play();
    }

    void PerformAbility()
    {
        GetComponent<Animation>().Play(enemy.GetAnimation(4));
    }

    void DoDamagePhysical()
    {
       enemy.DoDamagePhysical(t);

       target.GetComponent<Animation>().Play(t.GetAnimation(3));
    }

    void DoDamageMagical()
    {
        int a = Random.Range(0, enemy.GetAbilitiesLength());

        enemy.DoDamageMagical(t, enemy.GetAbility(ability_num));

        particle_current = enemy.GetAbility(ability_num).GetAbilityParticle();

        particle_current = Instantiate(particles[ability_num],
            new Vector3(target.transform.position.x,
          target.transform.position.y + 1,
          target.transform.position.z),
          Quaternion.identity) as ParticleSystem;

        particle_current.gameObject.AddComponent<ParticleSystemDestroyer>();
        ParticleSystemDestroyer pd = particle_current.GetComponent<ParticleSystemDestroyer>();
        pd.minDuration = pd.maxDuration = particle_current.time;

        if (t != enemy)
          target.GetComponent<Animation>().Play(t.GetAnimation(3));
    }

    void UpdatePlayerDeadStatus()
    {
        if (enemy.isDead && enemy.Health > 0)
        {
            bm.EnemiesDeadCounter(-1);
            GetComponent<Animation>().Play();
            enemy.isDead = false;
        }
        else if (!enemy.isDead && enemy.Health <= 0)
        {
            enemy.choice = "none";
            enemy.Health = 0;
            enemy.isDead = true;
            bm.EnemiesDeadCounter(1);
            GetComponent<Animation>().Play(enemy.GetAnimation(5));
        }
    }

    void Idle()
    {
        transform.rotation = origRot;
        transform.position = origPos;

        if (enemy.choice == "finishing" && particle_current == null)
            enemy.choice = "none";

        if (enemy.choice == "ability" && particle_current != null)
            enemy.choice = "finishing";

        if (inTransit && enemy.choice != "finishing")
        {
            enemy.choice = "none";
        }
        GetComponent<Animation>().Play();
    }


    //damage meter 
    public void CreateDamageMeterUI()
    {
        damage_meter_UI_current = Instantiate(damage_meter_UI, 
            new Vector3(transform.position.x, transform.position.y + 3.5f, transform.position.z),
          Quaternion.Euler(0, 180, 0)) as Canvas;

        Debug.Log(enemy.damage_meter);

        damage_meter_UI_current.GetComponentInChildren<Text>().text = enemy.damage_meter.ToString();
    }

    public void DestroyDamageMeterUI()
    {
        Destroy(damage_meter_UI_current, 2);
    }
}
