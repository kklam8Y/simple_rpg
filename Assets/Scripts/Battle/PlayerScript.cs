﻿using UnityEngine;
using System.Collections;
using System;
using UnityStandardAssets.Utility;

public class PlayerScript : MonoBehaviour {

    CharacterClass player, t;
    GameObject target;

    BattleManager bm;

    public ParticleSystem[] particles;
    ParticleSystem particle_current;

    Vector3 origPos;
    Quaternion origRot;

    int ability_num;
    public bool inTransit;
    public string targetType;

	// Use this for initialization
	void Start ()
    {
        origPos = transform.position;
        origRot = transform.rotation;

        bm = GameObject.FindGameObjectWithTag("BattleManager").GetComponent<BattleManager>();

        if (name == "unitychan")
            player = new FireMageClass(particles);


        if (name == "samuzai")
            player = new NinjaClass(particles);


        if (name == "weeb")
            player = new WeabooClass(particles);

        //player.GetInfor();
	}
	
	// Update is called once per frame
	void Update ()
    {
        UpdatePlayerDeadStatus();
	}

    public CharacterClass getCharacter() { return player; }

    public GameObject Target { get { return target; } set { target = value; } }

    public int AbilityNumber { set { ability_num = value; } }

    void PerformAttack()
    {
        if (player.GetRange() == 0)
        {
            if (Vector3.Distance(transform.position, target.transform.position) > 1.0)
            {
                Vector3 reletivePos = target.transform.position - transform.position;
                Quaternion rotation = Quaternion.LookRotation(reletivePos);
                Quaternion current = transform.localRotation;

                transform.localRotation = Quaternion.Slerp(current, rotation, 1 * Time.deltaTime);

                transform.position = Vector3.Lerp(transform.position, target.transform.position, Time.deltaTime);


                //walk/run animation
                GetComponent<Animation>().Play(player.GetAnimation(1));
            }
            else if (Vector3.Distance(transform.position, target.transform.position) < 1.0)
            {
                //attack animation
                GetComponent<Animation>().Play(player.GetAnimation(2));

            }
        }
        if (player.GetRange() == 1)
        {

            //attack animation
            GetComponent<Animation>().Play(player.GetAnimation(4));
        }
    }

    void PerformDefend()
    {
        GetComponent<Animation>().Play();
    }

    void PerformAbility()
    {
        GetComponent<Animation>().Play(player.GetAnimation(4));
    }

    public void ExecuteChoice()
    {
        if (target != null)
        {
            CheckTarget();
        }

        if (player.choice == "attack")
        {
            inTransit = true;
            PerformAttack();

        }
        else if (player.choice == "defend")
        {
            inTransit = true;
            PerformDefend();

        }
        else if (player.choice == "ability")
        {
            inTransit = true;
            PerformAbility();

        }
        else if (player.choice == "none")
        {
            inTransit = false;
        }

    }

    void DoDamagePhysical()
    {
        if (player.DoDamagePhysical(t))

        {
            if (t != player && !t.isDead)
                target.GetComponent<Animation>().Play(t.GetAnimation(3));

            //target.GetComponent<EnemyScript>().CreateDamageMeterUI();
        }
    }

    void DoDamageMagical()
    {
        AbilityClass ability = player.GetAbility(ability_num);

        int targetSelected = 0;

        if (targetType == "enemy")
            targetSelected = System.Array.IndexOf(bm.enemies, target);
        else if (targetType == "player")
            targetSelected = System.Array.IndexOf(bm.players, target);


        if (player.DoDamageMagical(t, ability))
        {
            if (ability.GetRadius > 0)
            {
                AOETargetsRight(targetSelected, ability);
                AOETargetsLeft(targetSelected, ability);
            }

            CreateAbilityParticleEffect(targetSelected);

            if (t != player && ability.GetType == "damage" && !t.isDead)
                target.GetComponent<Animation>().Play(t.GetAnimation(3));
        }
    }


    void CheckTarget()
    {
        if (targetType == "player")
            t = target.GetComponent<PlayerScript>().getCharacter();
        if (targetType == "enemy")
            t = target.GetComponent<EnemyScript>().getCharacter();
        if (t.isDead && player.choice == "attack")
        {
            player.choice = "none";
            Debug.Log("target is dead");
        }
    }

    //return to idle after execute
    void Idle()
    {
        transform.rotation = origRot;
        transform.position = origPos;

        if ((player.choice == "finishing" && particle_current == null))
            player.choice = "none";

        if (player.choice == "ability" && particle_current != null)
            player.choice = "finishing";

        if (inTransit && player.choice != "finishing")
        {
            player.choice = "none";
            //target.GetComponent<EnemyScript>().DestroyDamageMeterUI();
        }
        GetComponent<Animation>().Play();
    }

    void UpdatePlayerDeadStatus()
    {
              if (player.isDead && player.Health > 0)
        {
            bm.PlayersDeadCounter(-1);
            GetComponent<Animation>().Play();
            player.isDead = false;
        }
        else if (!player.isDead && player.Health <= 0)
        {
            player.choice = "none";
            player.Health = 0;
            player.isDead = true;
            bm.PlayersDeadCounter(1);
            GetComponent<Animation>().Play(player.GetAnimation(5));
        }
    }

    /* couldnt find a way to play the animation from a spesific keyframe in c#
    is only possible in jave */
    void DontPlayDeadAnimationOnStart()
    {
        if (GetComponent<Animation>().IsPlaying(player.GetAnimation(0)))
        {

        }
    }


    void CreateAbilityParticleEffect(int target_index)
    {
        Vector3 target_pos = Vector3.zero;

        if(targetType == "enemy")
            target_pos = bm.enemies[target_index].transform.position;
        else if(targetType == "player")
            target_pos = bm.players[target_index].transform.position;

        particle_current = Instantiate(particles[ability_num],
          new Vector3(target_pos.x, target_pos.y + 1, target_pos.z),
        Quaternion.identity) as ParticleSystem;

        particle_current.gameObject.AddComponent<ParticleSystemDestroyer>();
        ParticleSystemDestroyer pd = particle_current.GetComponent<ParticleSystemDestroyer>();
        pd.minDuration = pd.maxDuration = particle_current.time;
    }

    //For Abilities with AOE
    void AOETargetsRight(int target_index, AbilityClass ability)
    {
        target_index += 1;

        for (int i = 0; i < ability.GetRadius; i++)
        {
            CharacterClass c = null;

                if (targetType == "enemy" && target_index < bm.enemies.Length)
                {
                     c = bm.enemies[target_index].GetComponent<EnemyScript>().getCharacter();

                     if (c != null)
                     {
                         if(player.DoDamageMagical(c, ability))
                         {
                             if (ability.GetType == "damage" && !c.isDead)
                                 bm.enemies[target_index].GetComponent<Animation>().Play(c.GetAnimation(3));

                             CreateAbilityParticleEffect(target_index);
                         }
                     }
                }

                if (targetType == "player" && target_index < bm.enemies.Length)
                {
                    c = bm.players[target_index].GetComponent<PlayerScript>().getCharacter();

                    if (c != null)
                    {
                        if(player.DoDamageMagical(c, ability))
                        {
                            if (ability.GetType == "damage" && !c.isDead)
                                bm.players[target_index].GetComponent<Animation>().Play(c.GetAnimation(3));

                            CreateAbilityParticleEffect(target_index);
                        }
                    }
                }
                Debug.Log(target_index);
            
            target_index++;
        }
    }

    void AOETargetsLeft(int target_index, AbilityClass ability)
    {
        target_index -= 1;


        for (int i = 0; i > -ability.GetRadius; i--)
        {
            CharacterClass c = null;

            if (targetType == "enemy" && target_index>=0)
            {
                c = bm.enemies[target_index].GetComponent<EnemyScript>().getCharacter();

                if (c != null)
                {
                    if(player.DoDamageMagical(c, ability))
                    {
                        if (ability.GetType == "damage" && !c.isDead)
                            bm.enemies[target_index].GetComponent<Animation>().Play(c.GetAnimation(3));

                        CreateAbilityParticleEffect(target_index);
                    }
                }
            }

            if (targetType == "player" &&  target_index >= 0)
            {
                c = bm.players[target_index].GetComponent<PlayerScript>().getCharacter();

                if (c != null)
                {
                    if(player.DoDamageMagical(c, ability))
                    {
                        if (ability.GetType == "damage" && !c.isDead)
                            bm.players[target_index].GetComponent<Animation>().Play(c.GetAnimation(3));

                        CreateAbilityParticleEffect(target_index);
                    }
                }
            }
            Debug.Log(target_index);

            target_index++;
        }
    }


    void AOEOnPlayers(AbilityClass ability, int target_index)
    {
        CharacterClass c = bm.players[target_index].GetComponent<PlayerScript>().getCharacter();

        //Debug.Log(target_index);

        if (c != null)
        {
            if(player.DoDamageMagical(c, ability))
            {
                if (ability.GetType == "damage" && !c.isDead)
                    bm.players[target_index].GetComponent<Animation>().Play(c.GetAnimation(3));

                CreateAbilityParticleEffect(target_index);
            }
        }
        target_index--;
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.tag == "enemy")
            GetComponent<Animation>().Play(player.GetAnimation(2));
    }

    //save player data when the gameobjects are destroyed on scene transition
    void OnDestroy()
    {
        PlayerPrefs.SetInt("money", PlayerPrefs.GetInt("money") + bm.gold_drop);

        if (player.ID == "plr_unity")
        {
            PlayerPrefs.SetFloat("unitychan_hp", player.Health);
            //Debug.Log(PlayerPrefs.GetInt("unitychan_hp"));
            PlayerPrefs.SetInt("unitychan_exp", player.Experience);
            PlayerPrefs.SetInt("unitychan_lvl", player.Level);
            Debug.Log(PlayerPrefs.GetInt("unitychan_lvl"));
            Debug.Log(PlayerPrefs.GetInt("unitychan_exp"));
        }
        if (player.ID == "plr_samuzai")
        {
            PlayerPrefs.SetFloat("samuzai_hp", player.Health);
            //Debug.Log(PlayerPrefs.GetInt("samuzai_hp"));
            PlayerPrefs.SetInt("samuzai_exp", player.Experience);
            PlayerPrefs.SetInt("samuzai_lvl", player.Level);
        }
        if (player.ID == "plr_tai")
        {
            PlayerPrefs.SetFloat("taichi_hp", player.Health);
            //Debug.Log(PlayerPrefs.GetInt("taichi_hp"));
            PlayerPrefs.SetInt("taichi_exp", player.Experience);
            PlayerPrefs.SetInt("taichi_lvl", player.Level);
        }
    }
}
