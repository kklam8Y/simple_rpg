﻿using UnityEngine;
using System.Collections;

public class FireMageClass : CharacterClass {

    public FireMageClass(ParticleSystem[] ps)
    {
        level = PlayerPrefs.GetInt("unitychan_lvl");
        experience = PlayerPrefs.GetInt("unitychan_exp");

        name = "UnityChan";
        id = "plr_unity";

        StatisticPerLevelGrowth();

        health = PlayerPrefs.GetFloat("unitychan_hp");
        //mana = 0;

        //animations
        animationsNames = new string[6];

        animationsNames[0] = "WAIT01";
        animationsNames[1] = "WALK00_F";
        animationsNames[2] = "SLIDE00";
        animationsNames[3] = "DAMAGED00";
        animationsNames[4] = "REFLESH00";
        animationsNames[5] = "DAMAGED01";

        //abilitiese
        abilities = new AbilityClass[ps.Length];

        abilities[0] = new AbilityClass("fireblast", "damage", 10, 30, ps[0]);
        abilities[1] = new AbilityClass("heal", "heal", 5, 10, ps[1]);
        abilities[2] = new AbilityClass("healing wave", "heal", 5, 20, 1, ps[1]);  
    }
    public override void StatisticPerLevelGrowth()
    {
        strength = 3 + level * 1;
        agility = 5 + level * 1;
        intellect = 7 + level * 5;
        spirit = 6 + level * 4;
        vitality = 4 + level * 2;

        attack = strength/2 + agility/2 + intellect;
        m_attack = intellect + spirit / 2;

        p_resist = strength / 4 + agility / 4;
        m_resist = intellect + spirit / 2;

        base.StatisticPerLevelGrowth();
    }
}
