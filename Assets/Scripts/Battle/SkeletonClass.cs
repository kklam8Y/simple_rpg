﻿using UnityEngine;
using System.Collections;

public class SkeletonClass : CharacterClass {

    public SkeletonClass(BattleManager bm, ParticleSystem[] ps, int size) : base(bm)
    {
        this.bm = bm;

        abilities = new AbilityClass[size];

        abilities[0] = new AbilityClass("bone chill", "damage", 5, 15, ps[0]);
        abilities[1] = new AbilityClass("heal", "heal", 10, 10, ps[1]);

        level = 1;

        name = "Skeleton Warrior";
        id = "enm_skel";

        strength = 5;
        agility = 3;
        intellect = 0;
        spirit = 2;
        vitality = 4;

        StatisticPerLevelGrowth();

        attack = strength  + 5;

        p_resist = strength;
        m_resist = intellect;
        evasion = 4.00f;
        speed = agility;
        critical = 2.0f;

        animationsNames = new string[6];

        animationsNames[0] = "Idle";
        animationsNames[1] = "Walk";
        animationsNames[2] = "Attack";
        animationsNames[3] = "Damage";
        animationsNames[4] = "Skill";
        animationsNames[5] = "Death";

    }

    public override CharacterClass GetLogic(int current)
    {
        int t = Random.Range(0, bm.GetPlayersCount);
        CharacterClass target = bm.players[t].GetComponent<PlayerScript>().getCharacter();

        int c = Random.Range(0, 100);

        if (health <= max_health / 2)
        {
            int h = Random.Range(0, 100);

            if (Mana >= GetAbility(1).GetCost && h < 80)
            {
                choice = "ability";
                bm.current_ability_selected = 1;

                bm.enemies[current].GetComponent<EnemyScript>().targetType = "enemy";
                bm.enemies[current].GetComponent<EnemyScript>().Target = bm.enemies[current];
            }
            else
            {
                choice = "attack";
                bm.enemies[current].GetComponent<EnemyScript>().targetType = "player";
                bm.enemies[current].GetComponent<EnemyScript>().Target = bm.players[t];
            }
        }
        else
        {
            if (c < 60)
                choice = "attack";
            else if (c > 60 && c < 80)
                choice = "defend";
            else if (c > 80)
            {
                choice = "ability";
                bm.current_ability_selected = 0;
            }

            bm.enemies[current].GetComponent<EnemyScript>().targetType = "player";
            bm.enemies[current].GetComponent<EnemyScript>().Target = bm.players[t];
        }
        

        bm.enemies[current].GetComponent<EnemyScript>().AbilityNumber = bm.current_ability_selected;
   

        //Debug.Log(choice);


        return target;
    }

    public override int ExperienceDrop()
    {
        return 10;
    }

    public override int MoneyDrop()
    {
        return 20;
    }
}
