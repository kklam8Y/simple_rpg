﻿using UnityEngine;
using System.Collections;

public class NinjaClass : CharacterClass
{
    public NinjaClass(ParticleSystem[] ps)
    {
        level = PlayerPrefs.GetInt("samuzai_lvl");
        experience = PlayerPrefs.GetInt("samuzai_exp");

        name = "Samuzai";
        id = "plr_samuzai";

        StatisticPerLevelGrowth();

        health = PlayerPrefs.GetFloat("samuzai_hp");
        Mana = 100;

        animationsNames = new string[6];

        animationsNames[0] = "Idle";
        animationsNames[1] = "Walk";
        animationsNames[2] = "Attack";
        animationsNames[3] = "Hit";
        animationsNames[4] = "Ability";
        animationsNames[5] = "Dead";


        abilities = new AbilityClass[ps.Length];
        abilities[0] = new AbilityClass("lightning storm", "damage", 20, 5, 1, ps[0]);
    }

    public override void StatisticPerLevelGrowth()
    {

        strength = 4 + level * 3;
        agility = 7 + level * 4;
        intellect = 3 + level * 1;
        spirit = 5 + level * 2;
        vitality = 4 + level * 4;

        attack = strength / 2 + agility;
        m_attack = intellect/2 + spirit / 2;

        p_resist = strength/2  + agility/2 ;
        m_resist = intellect/2 + spirit / 2;

        base.StatisticPerLevelGrowth();
    }
}