﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class BattleManager : MonoBehaviour
{
    public GameObject pointer, targetPointer;

    CharacterClass player;

    public CharacterClass t;

    public bool isChoosingEnemy, isChoosingAbility;
    public string choice;

    public GameObject[] players;
    public GameObject[] enemies;

    public bool m_AxisVInUse, m_AxisHInUse;

    private enum STATE
    {
        PLAYER_TURN,
        ENEMY_TURN,
        SORT_PLAYER,
        SORT_ENEMY,
        EXECUTE_PLAYER,
        EXECUTE_ENEMY,
        WIN,
        LOSS,
        END
    };

    private int num_dead_enemies;
    private int num_dead_players;

    public int current_ability_selected;

    public int selected = 0;
    public int enemySelected = 0;
    public int actionSelected = 0;

    private STATE currentState;

    public float inputH, inputV;

    //check if the selected target is an enemy
    public bool targetEnemy;

    public int turn;

    //post battle rewards.
    public int gold_drop;
    public int exp_drop;

    void Awake()
    {
        //create the enemies and terrain based on location in the local/global game world
        GetComponent<InstantiateEnemiesTerrain>().Initiate();
    }

    // Use this for initialization
    void Start()
    {
        players = GameObject.FindGameObjectsWithTag("Player").OrderBy(c => c.transform.position.x).ToArray() ;//add all the player party members to the enemies array
        enemies = GameObject.FindGameObjectsWithTag("Enemy"); //add all the enemy party members to the enemies array

        //for testing
        foreach (GameObject player in players)
        {
            Debug.Log(player.GetComponent<PlayerScript>().getCharacter().Speed);
        }

     
        Debug.Log("players count: " + players.Length);
        Debug.Log("enemies count: " + enemies.Length);

        currentState = STATE.PLAYER_TURN;

        selected = 0;
        enemySelected = 0;
        actionSelected = 0;
        turn = 0;

        //num_dead_enemies = num_dead_players = 0;

        m_AxisHInUse = false;

        targetEnemy = true;
    }

    // Update is called once per frame
    void Update()
    {
        GameSpeedControl();

        inputH = Input.GetAxis("Horizontal");

        inputV = Input.GetAxis("Vertical");

        Debug.Log(currentState);

        if (currentState == STATE.PLAYER_TURN)
        {
            //Debug.Log("dead count players; " + num_dead_players);
            //get player input for each party member

            if (targetEnemy)
            {
                //if (enemies[Selected] != null)
                //{
                t = enemies[enemySelected].GetComponent<EnemyScript>().getCharacter();
                //}
            }
            else if (!targetEnemy)
            {
                //if (players[Selected] != null)
                //{
                t = players[enemySelected].GetComponent<PlayerScript>().getCharacter();
                //}
            }

            CheckSelected();
            DoSomethingPlayer();
            CheckSelected();

            SetIndicatorPosition();
        }
        else if (currentState == STATE.ENEMY_TURN)
        {
            //get ai player input for each party member
            CheckSelected();
            DoSomethingEnemy();
            CheckSelected();

        }
        else if (currentState == STATE.SORT_PLAYER)
        {
            //execute the commands based on each characters spe
            CheckSelected();
            //for testing 
            foreach (GameObject player in players)
            {
                //Debug.Log(player.GetComponent<PlayerScript>().getCharacter().Speed);
            }

            CheckSelected();
        }
        else if (currentState == STATE.SORT_ENEMY)
        {
            CheckSelected();
            foreach (GameObject enemy in enemies)
            {
               // Debug.Log(enemy.GetComponent<EnemyScript>().getCharacter().Speed);
            }
            CheckSelected();
        }

        else if (currentState == STATE.EXECUTE_PLAYER)
        {
            //execute the commands based on each characters speed
            CheckSelected();
            ExecuteTurnPlayer();
            CheckSelected();
        }
        else if (currentState == STATE.EXECUTE_ENEMY)
        {
            //execute the commands based on each characters speed
            CheckSelected();
            ExecuteTurnEnemy();
            CheckSelected();
        }
        else if (currentState == STATE.WIN)
        {
            //reset time scale when leaving the battle

            CalculateExperienceMoneyDrop();

            currentState = STATE.END;

        }
        else if (currentState == STATE.LOSS)
        {
            //when ai wins
            //Time.timeScale = 0;
            Application.LoadLevel("town 1");
            PlayerPrefs.SetFloat("pos_x", 245.4366f);
            PlayerPrefs.SetFloat("pos_y", 31.00828f);
            PlayerPrefs.SetFloat("pos_z", 177.0092f);
        }

        else if (currentState == STATE.END)
        {
            Time.timeScale = 1;

            if (Input.GetKeyDown(KeyCode.Joystick1Button0))
            {
                Application.LoadLevel(PlayerPrefs.GetString("location"));
            }
        }
    }

    int DoSomethingPlayer()
    {
        player = players[selected].GetComponent<PlayerScript>().getCharacter();

        Debug.Log("selected: " + selected);

        if (!isChoosingEnemy)
        {
            if (Input.GetKeyDown(KeyCode.Joystick1Button0))
            {
                ActionSelect();
            }

            ActionNavigation();
        }

        if (player.isDead == true)
        {
            actionSelected = 0;
            player.choice = "none";
            return selected++;
        }

        targetPointer.GetComponent<Renderer>().enabled = false;

        if (!isChoosingEnemy && Input.GetKeyDown(KeyCode.Joystick1Button0) && player.choice == "defend")
        {
            actionSelected = 0;
            return selected++;
        }

        if (isChoosingEnemy)
        {

            //Debug.Log("abilities count" + player.GetAbilitiesLength());

            if (player.GetAbilitiesLength() < 0 && player.choice == "ability")
            {
                
                isChoosingEnemy = false;
                isChoosingAbility = false;
                return selected;
            }

            if (isChoosingAbility)
            {
                AbilityNavigator();
                if (Input.GetKeyDown(KeyCode.Joystick1Button0))
                {
                    players[selected].GetComponent<PlayerScript>().AbilityNumber = current_ability_selected;
                    isChoosingAbility = false;
                }
                else if(Input.GetKeyDown(KeyCode.Joystick1Button1))
                {
                    current_ability_selected = 0;
                    isChoosingEnemy = false;
                    isChoosingAbility = false;
                    return selected;
                }
            }
            else
            {
                TargetWhich(ref enemySelected);

                if (targetEnemy)
                {
                    SelectTargetEnemy();
                    if (Input.GetKeyDown(KeyCode.JoystickButton0))
                    {
                        AbilityTargetEnemy();
                        return selected++;
                    }
                }
                else if (!targetEnemy)
                {
                    SelectTargetPlayer();
                    if (Input.GetKeyDown(KeyCode.JoystickButton0))
                    {
                        AbilityTargetPlayer();
                        return selected++;
                    }

                }
                if (Input.GetKeyDown(KeyCode.Joystick1Button1))
                {
                    isChoosingEnemy = false;
                    isChoosingEnemy = false;
                    return selected;
                }
            }
         
        }

        if (!isChoosingEnemy && Input.GetKeyDown(KeyCode.Joystick1Button0) && player.choice != "defend")
        {
            isChoosingEnemy = true;
            return selected;
        }
        else if (!isChoosingEnemy && Input.GetKeyDown(KeyCode.Joystick1Button0) && player.choice == "defend")
            return selected++;

        if (Input.GetKeyDown(KeyCode.Joystick1Button1) && selected > 0)
        {
            CharacterClass previousPlayer = players[selected - 1].GetComponent<PlayerScript>().getCharacter();
            int player_selected = System.Array.IndexOf(players, previousPlayer);

            //int place_holder = selected;

            //foreach (GameObject p in players)
            //{
            //    CharacterClass c = p.GetComponent<PlayerScript>().getCharacter();

            //    if (c.isDead)
            //        selected--;

            //    if (c == null)
            //        selected = place_holder;
            //}
   
            if (previousPlayer.isDead)
            {
                if (selected == 1)
                    return selected;

                if (previousPlayer != null)
                    selected -= 1;
            }
            actionSelected = 0;

            if (player_selected !=  0)
                selected--;
        }

        return selected;
    }

    int DoSomethingEnemy()
    {

        CharacterClass enemy = enemies[selected].GetComponent<EnemyScript>().getCharacter();

        if (enemy.isDead == true)
        {
            return selected++;
        }

        CharacterClass t = enemy.GetLogic(selected);

        Debug.Log(t);

        if (t.isDead)
            return selected;
        else
        {
            selected++;
        }
   

        return selected;
    }

    int ExecuteTurnPlayer()
    {
        PlayerScript p = players[selected].GetComponent<PlayerScript>();

        p.ExecuteChoice();

        if (!p.inTransit)
            return selected++;
        if (p.inTransit)
            return selected;

        return selected;
    }

    int ExecuteTurnEnemy()
    {
        EnemyScript e = enemies[selected].GetComponent<EnemyScript>();

        e.ExecuteChoice();

        if (!e.inTransit)
            return selected++;
        if (e.inTransit)
            return selected;

        return selected;
    }

    public int CheckSelected()
    {
        //check if the battle is over
        if (num_dead_players == players.Length)
        {
            currentState = STATE.LOSS;
        }
        if (num_dead_enemies == enemies.Length)
        {
            currentState = STATE.WIN;
        }


        //player turn processes
        if (currentState == STATE.PLAYER_TURN)
        {
            if (selected > players.Length - 1)
            {
                selected = 0;
                //enemySelected = 0;
                currentState = STATE.SORT_PLAYER;
            }
        }
        else if (currentState == STATE.SORT_PLAYER)
        {
            players = players.OrderByDescending(c => c.GetComponent<PlayerScript>().getCharacter().Speed).ToArray();
            currentState = STATE.EXECUTE_PLAYER;

        }
        else if (currentState == STATE.EXECUTE_PLAYER)
        {
            if (selected > players.Length - 1)
            {
                enemySelected = 0;

//                ChooseEnemyNextNotDead();
                selected = 0;
                currentState = STATE.ENEMY_TURN;
           

                players = players.OrderBy(c => c.transform.position.x).ToArray();
            }
        }
  
        //enemy turn processes
        else if (currentState == STATE.ENEMY_TURN)
        {
            if (selected > enemies.Length - 1)
            {
                selected = 0; enemySelected = 0;
                currentState = STATE.SORT_ENEMY;
                Debug.Log(selected);
            }
        }
        else if (currentState == STATE.SORT_ENEMY)
        {
            enemies = enemies.OrderByDescending(c => c.GetComponent<EnemyScript>().getCharacter().Speed).ToArray(); 
            currentState = STATE.EXECUTE_ENEMY;

        }
        else if (currentState == STATE.EXECUTE_ENEMY)
        {
            if (selected > enemies.Length - 1)
            {
                enemySelected = 0;

                //ChooseEnemyNextNotDead();

                currentState = STATE.PLAYER_TURN;
                selected = 0;
                current_ability_selected = 0;
                enemies = enemies.OrderBy(c => c.transform.position.x).ToArray();
                turn++;
                Debug.Log("end of turn: " + turn);
            }
        }

        return selected;
    }

    //show which player is selecting a choice
    public void SetIndicatorPosition()
    {
        pointer.transform.Rotate(new Vector3(10, 0, 0), 100 * Time.deltaTime);
        targetPointer.transform.Rotate(new Vector3(10, 0, 0), 100 * Time.deltaTime);

        CharacterClass player_selected = null;

        if(currentState == STATE.PLAYER_TURN)
            player_selected = players[selected].GetComponent<PlayerScript>().getCharacter();

        if (currentState == STATE.PLAYER_TURN && !isChoosingEnemy && !player_selected.isDead)
        {
            //player pointer
            pointer.transform.position = new Vector3(players[selected].transform.position.x,
                                                     players[selected].transform.position.y + 2.3f,
                                                     players[selected].transform.position.z);
            pointer.GetComponent<Renderer>().enabled = true;
            pointer.GetComponent<Renderer>().material.color = new Color(0.5f, 1.0f, 0.0f);

        }

        //dont show pointers when it is the enemies turn.
        if (currentState != STATE.PLAYER_TURN)
        {
            pointer.GetComponent<Renderer>().enabled = false;
            targetPointer.GetComponent<Renderer>().enabled = false;
        }
    }

    //Target selectors
    public int SelectTargetEnemy()
    {
        //for analog stick navigation
        if (inputH > 0.2 || inputH < -0.2)
        {
            if (m_AxisHInUse == false)
            {
                if (inputH < 0 && enemySelected > 0)
                {
                    m_AxisHInUse = true;
                    return enemySelected--;
                }
                else if (inputH > 0 && enemySelected < enemies.Length - 1)
                {
                    m_AxisHInUse = true;
                    return enemySelected++;
                }
            }

        }
        else if (inputH == 0)
        {
            m_AxisHInUse = false;
        }

        //enemy pointer
        targetPointer.transform.position = new Vector3(enemies[enemySelected].transform.position.x,
                                                       enemies[enemySelected].transform.position.y + 2,
                                                       enemies[enemySelected].transform.position.z);
        targetPointer.GetComponent<Renderer>().enabled = true;
        pointer.GetComponent<Renderer>().enabled = false;
        targetPointer.GetComponent<Renderer>().material.color = new Color(1.0f, 0.0f, 0.0f);

        return enemySelected;
    }

    int SelectTargetPlayer()
    {
        //for analog stick navigation
        if (inputH > 0.2 || inputH < -0.2)
        {
            if (m_AxisHInUse == false)
            {
                if (inputH < 0 && enemySelected > 0)
                {
                    enemySelected--;
                    m_AxisHInUse = true;
                }
                else if (inputH > 0 && enemySelected < players.Length - 1)
                {
                    enemySelected++;
                    m_AxisHInUse = true;
                }
            }

        }
        else if (inputH == 0)
        {
            m_AxisHInUse = false;
        }

        //enemy pointer
        targetPointer.transform.position = new Vector3(players[enemySelected].transform.position.x,
                                                       players[enemySelected].transform.position.y + 2.2f,
                                                       players[enemySelected].transform.position.z);
        targetPointer.GetComponent<Renderer>().enabled = true;
        pointer.GetComponent<Renderer>().enabled = false;
        targetPointer.GetComponent<Renderer>().material.color = new Color(0.0f, 0.50f, 1.0f);

        return enemySelected;
    }

    //navigators
    //allows teh player to navigate to the choice they want to buffer for each player
    int ActionNavigation()
    {
        //for analog stick navigation
        if (inputV > 0.2 || inputV < -0.2)
        {
            if (m_AxisVInUse == false)
            {
                if (inputV > 0 && actionSelected > 0)
                {
                    actionSelected--;
                    m_AxisVInUse = true;
                }
                else if (inputH < 0 && actionSelected < 2)
                {
                    actionSelected++;
                    m_AxisVInUse = true;
                }
            }

        }
        if (inputV == 0)
        {
            m_AxisVInUse = false;
        }

        return actionSelected;
    }

    public int AbilityNavigator()
    {
        //for analog stick navigation
        if (inputV > 0.2 || inputV < -0.2)
        {
            if (m_AxisVInUse == false)
            {
                if (inputV > 0 && current_ability_selected > 0)
                {
                    current_ability_selected--;
                    m_AxisVInUse = true;
                }
                else if (inputH < 0 && current_ability_selected <  player.GetAbilitiesLength())
                {
                    current_ability_selected++;
                    m_AxisVInUse = true;
                }
            }

        }
        if (inputV == 0)
        {
            m_AxisVInUse = false;
        }

        return current_ability_selected;
    }

    public void ActionSelect()
    {
        if (actionSelected == 0)
            player.choice = "attack";
        else if (actionSelected == 1)
            player.choice = "defend";
        else if (actionSelected == 2)
        {
            isChoosingAbility = true;
            player.choice = "ability";
        }
    }


    //speed up or down the game speed
    public void GameSpeedControl()
    {
        if (Input.GetKeyDown(KeyCode.Joystick1Button5))
            Time.timeScale--;
        else if (Input.GetKeyDown(KeyCode.Joystick1Button4))
            Time.timeScale++;
    }

    public bool TargetWhich(ref int enemySelected)
    {
        if (inputV > 0.2 || inputV < -0.2)
        {
            if (m_AxisVInUse == false)
            {
                if (inputV > 0)
                {
                    enemySelected = 0;
                    targetEnemy = true;
                    m_AxisVInUse = true;
                }
                else if (inputH < 0)
                {
                    enemySelected = 0;
                    targetEnemy = false;
                    m_AxisVInUse = true;
                }
            }

        }
        if (inputV == 0)
        {
            m_AxisVInUse = false;
        }

        return targetEnemy;
    }

    void AbilityTargetEnemy()
    {
        players[selected].GetComponent<PlayerScript>().targetType = "enemy";
        players[selected].GetComponent<PlayerScript>().Target = enemies[enemySelected];
        isChoosingEnemy = false;
        isChoosingAbility = false;
        current_ability_selected = 0;
        actionSelected = 0;
    }

    void AbilityTargetPlayer()
    {
        players[selected].GetComponent<PlayerScript>().targetType = "player";
        players[selected].GetComponent<PlayerScript>().Target = players[enemySelected];
        isChoosingEnemy = false;
        isChoosingAbility = false;
        current_ability_selected = 0;
        actionSelected = 0;
    }

    //returns the state curretly in action.
    public string GetCurrentState { get { return currentState.ToString(); } }
    public CharacterClass GetCurrentPlayerSelected()
    { 
        return  players[selected].GetComponent<PlayerScript>().getCharacter();
    }

    //counter of combatants that are not dead.
    public int GetPlayersCount { get { return players.Length; } }
    public int GetEnemiesCount { get { return enemies.Length; } }

    //death counters checks how many members of each party are alive;
    public int PlayersDeadCounter(int value)
    {
        return num_dead_players += value;
    }
    public int EnemiesDeadCounter(int value)
    {
        return num_dead_enemies += value;
    }

    void ChooseEnemyNextNotDead()
    {

        foreach (GameObject enemy in enemies)
        {
            t = enemies[enemySelected].GetComponent<EnemyScript>().getCharacter();

            if (t.isDead && t != null)
                enemySelected++;
        }

    }

    void CalculateExperienceMoneyDrop()
    {
        foreach (GameObject enemy in enemies)
        {
            CharacterClass e = enemy.GetComponent<EnemyScript>().getCharacter();

            gold_drop += e.MoneyDrop();
            exp_drop += e.ExperienceDrop();
        }

        foreach (GameObject player in players)
        {
            CharacterClass p = player.GetComponent<PlayerScript>().getCharacter();

            if(!p.isDead)
                p.Experience += exp_drop;

            if (p.ExperienceRequiredForLevelUp() <= p.Experience)
            {
                p.Experience = p.Experience - (int)p.ExperienceRequiredForLevelUp(); //leave the remainder of the experience counter to be put towards next level
                p.Level += 1;
            }
        }
    }

}

