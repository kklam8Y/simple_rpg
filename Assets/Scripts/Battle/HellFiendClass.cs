﻿using UnityEngine;
using System.Collections;

public class HellFiendClass : CharacterClass
{

    public HellFiendClass(BattleManager bm) : base(bm)
    {
        this.bm = bm;

        level = 1;

        name = "Hell Fiend";
        id = "enm_hellfiend";

        strength = 5;
        agility = 6;
        intellect = 0;
        spirit = 2;
        vitality = 2;

        StatisticPerLevelGrowth();

        //health = 2;

        attack = strength + 15;

        p_resist = strength;
        m_resist = intellect;
        evasion = 2.00f;
        speed = agility;
        critical = 20.00f;

        animationsNames = new string[6];

        animationsNames[0] = "Idle";
        animationsNames[1] = "Walk";
        animationsNames[2] = "Attack";
        animationsNames[3] = "Hit1";
       // animationsNames[4] = "Skill";
        animationsNames[5] = "Dead";

    }

    public override CharacterClass GetLogic(int current)
    {
        int range = bm.players.Length;

        int t = 0;

        for (int i = 1; i < bm.players.Length; i++)
        {
            float player_health = bm.players[t].GetComponent<PlayerScript>().getCharacter().Health;
            float next_player_health = bm.players[i].GetComponent<PlayerScript>().getCharacter().Health;

            bool dead_next = bm.players[i].GetComponent<PlayerScript>().getCharacter().isDead;
            bool dead_current = bm.players[t].GetComponent<PlayerScript>().getCharacter().isDead;

            if ((player_health > next_player_health) || dead_current)
            {
                if (!dead_next )
                {
                    t = i;
                }
            }
          
        }


        CharacterClass target = bm.players[t].GetComponent<PlayerScript>().getCharacter();

        int c = Random.Range(0, 100);
        if (c < 98)
            choice = "attack";
        else if (c > 98)
            choice = "defend";

        bm.enemies[current].GetComponent<EnemyScript>().targetType = "player";
        bm.enemies[current].GetComponent<EnemyScript>().Target = bm.players[t];


        bm.enemies[current].GetComponent<EnemyScript>().AbilityNumber = bm.current_ability_selected;


        //Debug.Log(choice);


        return target;
    }

    public override int ExperienceDrop()
    {
        return 5;
    }

    public override int MoneyDrop()
    {
        return 2;
    }
}

