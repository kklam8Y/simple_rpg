﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BattleGUIControl : MonoBehaviour {

   // GameManager bm;
    BattleManager bm;
   
    //canvases 
    public Canvas ChoicesCanvas;
    public Canvas TargetSelectorCanvas;
    public Canvas TargetDescription;
    public Canvas PlayerDescCanvas;
    public Canvas WinCanvas;

    public Image attackImg, defendImg, abilityImg;

    public Text targetName;
    public Text targetHealth;
    public Text targetMana;

    public Text WinText;
    public Text LossText;

    public Text MoneyReward;
    public Text ExpReward;

    public AudioSource select;
    public AudioSource cancel;

    public Canvas AbilitiesCanvas;
    public Text abilities_text;

    public Text[] players_description_texts;

	// Use this for initialization
	void Start () {
        bm = gameObject.GetComponent<BattleManager>();

        WinCanvas.enabled = false;
       
	}
	
	// Update is called once per frame
	void Update () {


        if (bm.GetCurrentState == "WIN")
        {
            WinCanvas.enabled = true;
            ChoicesCanvas.enabled = false;
            GetComponent<AudioSource>().Pause();
        }

        else if (bm.isChoosingEnemy && bm.GetCurrentState == "PLAYER_TURN")
        {
            ChoicesCanvas.enabled = false;
            TargetSelectorCanvas.enabled = true;
            TargetDescription.enabled = true;

            if (bm.t != null)
            {
                targetName.text = bm.t.Name.ToUpper();
                targetHealth.text = bm.t.MaxHealth.ToString() + "/" + bm.t.Health.ToString();
                targetMana.text = bm.t.MaxMana.ToString() + "/" + bm.t.Mana.ToString();
            }
        }
        else
        {
            ChoicesCanvas.enabled = true;
            TargetSelectorCanvas.enabled = false;
            TargetDescription.enabled = false;
        }

        attackImg.enabled = false;
        defendImg.enabled = false;
        abilityImg.enabled = false;

        if (bm.actionSelected == 0)
            attackImg.enabled = true;
        else if(bm.actionSelected == 1)
            defendImg.enabled = true;
        else if (bm.actionSelected == 2)
            abilityImg.enabled = true;

        if (bm.GetCurrentState == "PLAYER_TURN")
        {
            ChoicesCanvas.enabled = true;

            if (Input.GetKeyDown(KeyCode.Joystick1Button0))
                select.Play();
            if (Input.GetKeyDown(KeyCode.Joystick1Button1))
                cancel.Play();
        }
        else
            ChoicesCanvas.enabled = false;

        if (bm.isChoosingAbility)
        {
            AbilitiesCanvas.enabled = true;
            TargetSelectorCanvas.enabled = false;
            ChoicesCanvas.enabled = false;
            TargetDescription.enabled = false;


            //CharacterClass p = bm.players[bm.selected].GetComponent<PlayerScript>().getCharacter();

            abilities_text.text = bm.GetCurrentPlayerSelected().GetAbilityDetails(bm.current_ability_selected);

        }
        else
            AbilitiesCanvas.enabled = false;

        PlayerStatisticsUI();
  
        if(bm.GetCurrentState == "END")
        {
            ChoicesCanvas.enabled = false;
            TargetSelectorCanvas .enabled = false;
            TargetDescription.enabled = false;
            PlayerDescCanvas.enabled = false;
        }

        MoneyReward.text = "Gold: " + "<color=orange>" + bm.gold_drop.ToString()  + "</color>";
        ExpReward.text = "Experience: " + "<color=teal>" + bm.exp_drop.ToString() + "</color>";
	}

    public void PlayerStatisticsUI()
    {
        CharacterClass c;

        int n = 0;

        if (bm.GetCurrentState != "EXECUTE_PLAYER")
        {

            for (int i = 0; i < 3; i++)
            {
                c = bm.players[i].GetComponent<PlayerScript>().getCharacter();
                players_description_texts[i].text = "<color=black>" + "lvl: " + c.Level + "</color>" + " " + "<b>" + c.Name + "</b>";
            }

            for (int i = 3; i < 6; i++)
            {
                c = bm.players[n].GetComponent<PlayerScript>().getCharacter();

                players_description_texts[i].text = "<color=lime>" + c.MaxHealth.ToString() + "</color>" + "<color=black>" + "/" + "</color>" + c.Health.ToString();

                float health_threshhold = (c.MaxHealth / 100) * 20;

                //Debug.Log(health_threshhold);

                if (c.Health < health_threshhold)
                {
                    players_description_texts[i].color = Color.red;
                }
                else
                    players_description_texts[i].color = Color.green;
                n++;
            }

            n = 0;

            for (int i = 6; i < 9; i++)
            {
                c = bm.players[n].GetComponent<PlayerScript>().getCharacter();

                players_description_texts[i].text = "<color=blue>" + c.MaxMana.ToString() + "</color>" + "<color=black>" + "/" + "</color>" + c.Mana.ToString();

                float mana_threshhold = (c.MaxMana / 100) * 20;

                if (c.Mana < mana_threshhold)
                {
                    players_description_texts[i].color = Color.gray;
                }
                else
                    players_description_texts[i].color = Color.blue;

                n++;
            }
        }
    }
}
