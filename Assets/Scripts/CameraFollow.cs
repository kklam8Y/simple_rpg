﻿using UnityEngine;
using System.Collections;


//code obtained from http://stackoverflow.com/questions/10752435/how-do-i-make-a-camera-follow-an-object-in-unity3d-c
public class CameraFollow : MonoBehaviour
{

// The target we are following
public  Transform target;
// The distance in the x-z plane to the target
public float distance = 2.0f;
// the height we want the camera to be above the target
public float height = 1.0f;
// How much we 
public float heightDamping = 2.0f;
public float rotationDamping = 0.6f;

float
    distanceAway,
    distanceUp;

Vector3 velocityCamSmoothe = Vector3.zero;
float camSmoothDamptime = 0.1f;

Vector3
    lookDir,
    targetPos;

RaycastHit wallHit;

float distanceFromWall ;

void Start()
{
    wallHit = new RaycastHit();

    lookDir = target.transform.forward;
}

void  LateUpdate (){

    // Calculate the current rotation angles
    float wantedRotationAngle = target.eulerAngles.y;
    float wantedHeight = target.position.y + height;

    float currentRotationAngle = transform.eulerAngles.y;
    float currentHeight = transform.position.y;

    // Damp the rotation around the y-axis
    currentRotationAngle = Mathf.LerpAngle (currentRotationAngle, wantedRotationAngle, rotationDamping * Time.deltaTime);

    // Damp the height
    currentHeight = Mathf.Lerp (currentHeight, wantedHeight, heightDamping * Time.deltaTime);

    // Convert the angle into a rotation
    Quaternion currentRotation = Quaternion.Euler (0, currentRotationAngle, 0);

    // Set the position of the camera on the x-z plane to:
    // distance meters behind the target
    transform.position = target.position;
    transform.position -= currentRotation * Vector3.forward * distance;

    // Set the height of the camera
    transform.position = new Vector3(transform.position.x, currentHeight, transform.position.z);

    // Always look at the target
    transform.LookAt (target);

    Vector3 characterOffset = target.transform.position + new Vector3(0, distanceUp, 0);

    lookDir = characterOffset - transform.position;
    lookDir.y = 0;
    lookDir.Normalize();

    //prevention from going through walls copied from https://www.youtube.com/watch?v=MOoiezkQZmk

    Debug.DrawLine(transform.position, lookDir, Color.green);

    targetPos = characterOffset + target.up * distanceUp - lookDir * distanceAway;

    Debug.DrawLine(target.position, targetPos, Color.magenta);

    Debug.DrawLine(target.position, transform.position, Color.cyan);

    if (Physics.Linecast(target.position, transform.position, out wallHit))
    {
        Debug.Log("hit");
        transform.position = new Vector3(wallHit.point.x, transform.position.y, wallHit.point.z);
    }

    transform.LookAt(characterOffset);
}


}