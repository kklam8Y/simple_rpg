﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LocalGUIControl : MonoBehaviour {


    public Canvas Interaction;
    public Text Money_Text;
    public Image Interaction_img;
    public Text Interact_text;
    public Canvas SafeCanvas;

    Interact i;
    SceneTransition st;

	// Use this for initialization
	void Start () {
        i = gameObject.GetComponent<Interact>().getInteract;
        st = gameObject.GetComponent<SceneTransition>();
	}
	
	// Update is called once per frame
	void Update () {

        Money_Text.text = PlayerPrefs.GetInt("money").ToString();

        if (i.Hit)
            Interaction.enabled = true;
        else if (!i.Hit)
            Interaction.enabled = false;

        if(st.inSafeZone)
            SafeCanvas.enabled = true;
        else
            SafeCanvas.enabled = false;


        if (i.Object_Hit == "entrance")
            Interact_text.text = "Traverse";
        else if (i.Object_Hit == "chest")
            Interact_text.text = "Loot";
        else if(i.Object_Hit == "NPC")
            Interact_text.text = "Talk";
     
	}
}
