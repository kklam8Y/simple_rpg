﻿using UnityEngine;
using System.Collections;

public class Interact : MonoBehaviour {

    private float money_total;

    public LineRenderer line;
    public AnimationCurve money_roll;

    public float rayLength = 10.0f;
    public float rayHeight = 1.0f;

    private bool hit;
    private string object_hit;

    public Interact getInteract { get { return this; } }

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
        
        Quaternion q = transform.rotation;
        Ray ray = new Ray(transform.position + Vector3.up * rayHeight, q * Vector3.forward);
        line.SetPosition(0, ray.origin);
        line.SetPosition(1, ray.GetPoint(rayLength));// draw a ray so we can see what we are looking a

        CheckRayToChest(ray);

        Debug.Log(object_hit);
	}

    //chekc if the ray hit a chest
    void CheckRayToChest ( Ray ray)
    {
        RaycastHit rayhit;// just needed for testing

        if (Physics.Raycast(ray, out rayhit, rayLength))
        {

            if (rayhit.collider.tag == "Chest")
            {
                hit = true;

                object_hit = "chest";

                if (Input.GetKeyDown(KeyCode.Joystick1Button0))
                {
                    rayhit.collider.tag = "Untagged";

                    money_total = (CurveWeightedRandom(money_roll) * 100);

                    PlayerPrefs.SetInt("money", PlayerPrefs.GetInt("money") + (int)money_total);
                }

            }
            else if (rayhit.collider.tag == "NPC")
            {
                hit = true;

                object_hit = "NPC";

                if (Input.GetKeyDown(KeyCode.Joystick1Button0))
                {
                    //todo read from file
                    Debug.Log("please bring my bother back safe," + "\n" + "he's somewhere inside that" + "<b>" + " cave" + "</b>");
                }

            }
            else if (rayhit.collider.tag == "Entrance")
            {
                hit = true;

                object_hit = "entrance";

                if (rayhit.collider.name == "Cave 1 Exit" &&
                    Input.GetKey(KeyCode.Joystick1Button0))
                {

                    PlayerPrefs.SetFloat("pos_x", 187.572f);
                    PlayerPrefs.SetFloat("pos_y", 30.7817f);
                    PlayerPrefs.SetFloat("pos_z", 201.6865f);

                    PlayerPrefs.SetFloat("rot_x", 0);
                    PlayerPrefs.SetFloat("rot_y", 20.96338f);
                    PlayerPrefs.SetFloat("rot_z", 0);

                    Application.LoadLevel("town 1");
                }
                else if (rayhit.collider.name == "Cave 1 Entrance" &&
                  Input.GetKey(KeyCode.Joystick1Button0))
                {
                    PlayerPrefs.SetFloat("pos_x", 477.2301f);
                    PlayerPrefs.SetFloat("pos_y", 2.553159f);
                    PlayerPrefs.SetFloat("pos_z", 432.147f);

                    PlayerPrefs.SetFloat("rot_x", 0);
                    PlayerPrefs.SetFloat("rot_y", 265.7239f);
                    PlayerPrefs.SetFloat("rot_z", 0);

                    Application.LoadLevel("cave 1");
                }
            }
            else
            {
                hit = false;
                object_hit = null;
            }

        }
        else
            hit = false;
    }

    float CurveWeightedRandom(AnimationCurve curve)
    {
        return curve.Evaluate(Random.value);
    }

    public bool Hit { get { return hit; } }
    public string Object_Hit { get { return object_hit; } }

}
