﻿using UnityEngine;
using System.Collections;

public class CameraControlCave : MonoBehaviour
{

    public GameObject[] Cameras;
    private GameObject Player;

    public Camera Main_Camera;

    bool switch_cam;

    float
        distanceAway,
        distanceUp;

    Vector3 velocityCamSmoothe = Vector3.zero;
    float camSmoothDamptime = 0.1f;

    Vector3
        lookDir,
        targetPos;


    // Use this for initialization
    void Start()
    {
        Player = GameObject.FindGameObjectWithTag("Player");
        switch_cam = true;
    }

    // Update is called once per frame
    void Update()
    {

        Cameras = GameObject.FindGameObjectsWithTag("Event Camera");
        float distance = 100;
        Vector3 position = Player.transform.position;
        foreach (GameObject c in Cameras)
        {
            Vector3 diff = c.transform.position - position;
            float curDistance = diff.sqrMagnitude;
            if (curDistance < distance && switch_cam == true)
            {
                Main_Camera.enabled = false;
                c.GetComponent<Camera>().enabled = true;
                if (Input.GetKey(KeyCode.Joystick1Button0))
                {
                    if (switch_cam == true)
                        switch_cam = false;
                }
            }
            else
            {
                switch_cam = true;
                Main_Camera.enabled = true;
                c.GetComponent<Camera>().enabled = false;
            }
        }

    }
}
