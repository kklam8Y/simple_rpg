﻿using UnityEngine;
using System.Collections;

public class ParticleTrailControl : MonoBehaviour {

    public float verticle_speed = 1;
    public float amplitide = 0.5f;

    Renderer ren;
    Material mat;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
    void Update()
    {

        float t = 0;
        verticle_speed = 1;
        amplitide = 0.01f;

        t = Mathf.Sin(Time.time * verticle_speed) * amplitide;

        transform.position = new Vector3(transform.position.x, transform.position.y + t, transform.position.z);


    }
}
