﻿using UnityEngine;
using System.Collections;

public class SceneTransition : MonoBehaviour {

    private float threat = 0;
    public bool inSafeZone;
    public AnimationCurve curve;

    float inputV;
    float counter;

    void Awake()
    {
        threat = 0;
        counter = 0;
        Time.timeScale = 1;
    }

    void Update()
    {
        if (CheckMovement() && GeneratedThreat() > 80 && !inSafeZone)
        {
            StorePosition();
            Application.LoadLevel("battle scene");
        }

        if (Input.GetKeyDown(KeyCode.Joystick1Button4))
        {
            PlayerPrefs.SetInt("unitychan_lvl", 3);
            PlayerPrefs.SetInt("samuzai_lvl", 3);
            PlayerPrefs.SetInt("taichi_lvl", 3);

            PlayerPrefs.SetFloat("unitychan_hp", 100);
            PlayerPrefs.SetFloat("samuzai_hp", 100);
            PlayerPrefs.SetFloat("taichi_hp", 100);

            PlayerPrefs.SetFloat("pos_x", 245.4366f);
            PlayerPrefs.SetFloat("pos_y", 31.00828f);
            PlayerPrefs.SetFloat("pos_z", 177.0092f);

            PlayerPrefs.SetFloat("rot_x", 0);
            PlayerPrefs.SetFloat("rot_y", 20.96338f);
            PlayerPrefs.SetFloat("rot_z", 0);

            Application.LoadLevel("town 1");

            Debug.Log("Reset!");
        }
    }

    void OnLevelWasLoaded()
    {

        transform.position = GetPosition();
        transform.eulerAngles = GetRotation();

        if (Application.loadedLevelName == "town 1")
        {
            PlayerPrefs.SetString("location", "town 1");
        }

        if (Application.loadedLevelName == "cave 1")
        {
            PlayerPrefs.SetString("location", "cave 1");
        }
    }

    float CurveWeightedRandom(AnimationCurve curve)
    {
        return curve.Evaluate(Random.value);
    }

    public bool CheckMovement()
    {
        inputV = Input.GetAxis("Vertical");

        bool isMoving = false;

        if (inputV > 0.2 || inputV < -0.2)
            isMoving = true;

        return isMoving;
    }

    public float GeneratedThreat()
    {

        counter += Time.deltaTime;

        if (counter > 1)
        {
            threat = CurveWeightedRandom(curve) * 100;
            counter = 0;
        }

        return threat;
    }

    void OnTriggerStay(Collider col)
    {
        Debug.Log("safe");

        if (col.tag == "SafeZone")
        {
            inSafeZone = true;
            PlayerPrefs.SetString("location", "town 1");
        }
    }

    void OnTriggerExit(Collider col)
    {
        Debug.Log("not safe");
            inSafeZone = false;
    }

    public void StorePosition()
    {
        PlayerPrefs.SetFloat("pos_x", transform.position.x);
        PlayerPrefs.SetFloat("pos_y", transform.position.y);
        PlayerPrefs.SetFloat("pos_z", transform.position.z);

        PlayerPrefs.SetFloat("rot_x", transform.eulerAngles.x);
        PlayerPrefs.SetFloat("rot_y", transform.eulerAngles.y);
        PlayerPrefs.SetFloat("rot_z", transform.eulerAngles.z);
    }
    public Vector3 GetPosition() 
    { 
        return new Vector3(
            PlayerPrefs.GetFloat("pos_x"),
            PlayerPrefs.GetFloat("pos_y"),
            PlayerPrefs.GetFloat("pos_z"));
    }

    public Vector3 GetRotation()
    { 
        return new Vector3( 
            PlayerPrefs.GetFloat("rot_x"),
            PlayerPrefs.GetFloat("rot_y" ),
            PlayerPrefs.GetFloat("rot_z"));
    }
 }
