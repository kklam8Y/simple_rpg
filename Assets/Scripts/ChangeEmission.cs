﻿using UnityEngine;
using System.Collections;

public class ChangeEmission : MonoBehaviour {

    Renderer ren;
    Material mat;

    float floor = 0.1f;
    float ceiling = 0.2f;

    public float intensity, color;

	// Use this for initialization
	void Start () {
        ren = GetComponent<Renderer>();
        mat = ren.material;
	}
	
	// Update is called once per frame
	void Update () {

        if (tag == "Chest")
        {
            float emission = Mathf.Sin(Time.time * 10.0f) * (ceiling - floor);
            Color baseColor = new Color(0.06580061f, 0.1985294f, 0.04817258f); //Replace this with whatever you want for your base color at emission level '1'

            Color finalColor = baseColor * Mathf.LinearToGammaSpace(emission);

            mat.SetColor("_EmissionColor", finalColor);
        }
        else 
             mat.SetFloat("_Metallic", 0.0f);
	}
}
