﻿using UnityEngine;
using System.Collections;

public class CaveLightsControl : MonoBehaviour {

    public Light directional, point;
    private GameObject Player;

    public  GameObject[] Entrances;

    private float distance;

    float smooth = 1;

	// Use this for initialization
	void Start () {
        Player = GameObject.FindGameObjectWithTag("Player");
        Entrances = GameObject.FindGameObjectsWithTag("Entrance");

        directional.intensity = 0.1f;
	}
	
	// Update is called once per frame
	void Update () {
        float distance = 100;
        Vector3 position = Player.transform.position;

        foreach (GameObject Entrance in Entrances)
        {
            distance = 100;
        
            Vector3 diff = Entrance.transform.position - position;
            float curDistance = diff.sqrMagnitude;
            if (curDistance < distance && curDistance < 500)
            {
                if (directional.intensity < 2.0f)
                    directional.intensity = Mathf.Lerp(directional.intensity, 2.0f, Time.deltaTime * smooth);
                if (point.intensity > 0)
                    point.intensity = Mathf.Lerp(point.intensity, 0.0f, Time.deltaTime * 100);
            }
            else if(curDistance > distance && curDistance < 500)
            {
                if (directional.intensity > 0.1f)
                    directional.intensity = Mathf.Lerp(directional.intensity, 0.1f, Time.deltaTime * smooth);
                if (point.intensity < 3.0f)
                    point.intensity = Mathf.Lerp(point.intensity, 3.0f, Time.deltaTime * smooth);

            }
        }
	}


}
